
//----------------------------------------------------
// The following code was generated by CUP v0.10k
// Tue Mar 22 19:15:50 ECT 2022
//----------------------------------------------------

package sintactico;

import java_cup.runtime.Symbol;
import errors.*;
import utilities.Utilities;
import java.util.ArrayList;
import models.*;

/** CUP v0.10k generated parser.
  * @version Tue Mar 22 19:15:50 ECT 2022
  */
public class Sintax
 extends java_cup.runtime.lr_parser
{
  /** Default constructor. */
  public Sintax() {super();}

  /** Constructor which sets the default scanner. */
  public Sintax(java_cup.runtime.Scanner s) {super(s);}

  /** Production table. */
  protected static final short _production_table[][] = 
    unpackFromStrings(new String[] {
    "\000\020\000\002\002\004\000\002\003\004\000\002\003" +
    "\003\000\002\004\004\000\002\007\005\000\002\005\005" +
    "\000\002\005\003\000\002\010\004\000\002\010\003\000" +
    "\002\011\007\000\002\011\005\000\002\011\006\000\002" +
    "\006\004\000\002\006\007\000\002\006\005\000\002\006" +
    "\006" });

  /** Access to production table. */
  public short[][] production_table() {return _production_table;}

  /** Parse-action table. */
  protected static final short[][] _action_table = 
    unpackFromStrings(new String[] {
    "\000\043\000\006\004\004\011\005\001\002\000\004\011" +
    "\005\001\002\000\004\005\021\001\002\000\006\013\036" +
    "\015\035\001\002\000\010\006\031\013\ufff9\015\ufff9\001" +
    "\002\000\004\002\030\001\002\000\006\002\uffff\011\005" +
    "\001\002\000\006\013\015\015\014\001\002\000\006\002" +
    "\ufff5\011\ufff5\001\002\000\004\012\023\001\002\000\004" +
    "\011\016\001\002\000\006\005\021\012\023\001\002\000" +
    "\004\011\020\001\002\000\006\005\021\012\023\001\002" +
    "\000\010\006\ufffe\013\ufffe\015\ufffe\001\002\000\006\002" +
    "\ufff8\011\ufff8\001\002\000\004\005\024\001\002\000\004" +
    "\013\025\001\002\000\006\002\ufffd\011\ufffd\001\002\000" +
    "\006\002\ufff6\011\ufff6\001\002\000\006\002\ufff7\011\ufff7" +
    "\001\002\000\004\002\001\001\002\000\010\010\033\013" +
    "\ufffb\015\ufffb\001\002\000\006\013\ufffa\015\ufffa\001\002" +
    "\000\004\016\034\001\002\000\006\013\ufffc\015\ufffc\001" +
    "\002\000\004\012\023\001\002\000\004\011\037\001\002" +
    "\000\006\005\021\012\023\001\002\000\004\011\041\001" +
    "\002\000\004\012\023\001\002\000\006\002\ufff4\011\ufff4" +
    "\001\002\000\006\002\ufff2\011\ufff2\001\002\000\006\002" +
    "\ufff3\011\ufff3\001\002\000\006\002\000\011\005\001\002" +
    "" });

  /** Access to parse-action table. */
  public short[][] action_table() {return _action_table;}

  /** <code>reduce_goto</code> table. */
  protected static final short[][] _reduce_table = 
    unpackFromStrings(new String[] {
    "\000\043\000\012\003\007\004\006\006\010\010\005\001" +
    "\001\000\010\004\006\006\044\010\005\001\001\000\002" +
    "\001\001\000\002\001\001\000\004\005\031\001\001\000" +
    "\002\001\001\000\010\004\006\010\011\011\012\001\001" +
    "\000\002\001\001\000\002\001\001\000\004\007\026\001" +
    "\001\000\010\004\006\006\016\010\005\001\001\000\004" +
    "\007\025\001\001\000\010\004\006\010\011\011\012\001" +
    "\001\000\004\007\021\001\001\000\002\001\001\000\002" +
    "\001\001\000\002\001\001\000\002\001\001\000\002\001" +
    "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
    "\002\001\001\000\004\007\043\001\001\000\010\004\006" +
    "\010\011\011\037\001\001\000\004\007\042\001\001\000" +
    "\002\001\001\000\004\007\041\001\001\000\002\001\001" +
    "\000\002\001\001\000\002\001\001\000\010\004\006\010" +
    "\011\011\012\001\001" });

  /** Access to <code>reduce_goto</code> table. */
  public short[][] reduce_table() {return _reduce_table;}

  /** Instance of action encapsulation class. */
  protected CUP$Sintax$actions action_obj;

  /** Action encapsulation object initializer. */
  protected void init_actions()
    {
      action_obj = new CUP$Sintax$actions(this);
    }

  /** Invoke a user supplied parse action. */
  public java_cup.runtime.Symbol do_action(
    int                        act_num,
    java_cup.runtime.lr_parser parser,
    java.util.Stack            stack,
    int                        top)
    throws java.lang.Exception
  {
    /* call code in generated class */
    return action_obj.CUP$Sintax$do_action(act_num, parser, stack, top);
  }

  /** Indicates start state. */
  public int start_state() {return 0;}
  /** Indicates start production. */
  public int start_production() {return 0;}

  /** <code>EOF</code> Symbol index. */
  public int EOF_sym() {return 0;}

  /** <code>error</code> Symbol index. */
  public int error_sym() {return 1;}



    private Symbol s;
    
    public void syntax_error(Symbol s){
        this.s = s;
    }

    public Symbol getS(){
        return this.s;
    }

}

/** Cup generated class to encapsulate user supplied action code.*/
class CUP$Sintax$actions {


    private Integer type;
    private String T_data;
    public ArrayList<Etiqueta> etiquetas = new ArrayList<Etiqueta>();   
    public ArrayList<EtiquetaAux> str_etq = new ArrayList<EtiquetaAux>();
    public int nivela = 0; 
    public int nivelc = 0; 

  private final Sintax parser;

  /** Constructor */
  CUP$Sintax$actions(Sintax parser) {
    this.parser = parser;
  }

  /** Method with the actual generated action code. */
  public final java_cup.runtime.Symbol CUP$Sintax$do_action(
    int                        CUP$Sintax$act_num,
    java_cup.runtime.lr_parser CUP$Sintax$parser,
    java.util.Stack            CUP$Sintax$stack,
    int                        CUP$Sintax$top)
    throws java.lang.Exception
    {
      /* Symbol object for return from actions */
      java_cup.runtime.Symbol CUP$Sintax$result;

      /* select the action based on the action number */
      switch (CUP$Sintax$act_num)
        {
          /*. . . . . . . . . . . . . . . . . . . .*/
          case 15: // ELEMENTO ::= ETIQUETA_INICIO Cierre Apertura CIERRE_ETIQUETA 
            {
              Object RESULT = null;
		
    Utilities.verifyCloseEtq(etiquetas, str_etq);

              CUP$Sintax$result = new java_cup.runtime.Symbol(4/*ELEMENTO*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-3)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 14: // ELEMENTO ::= ETIQUETA_INICIO Texto CIERRE_ETIQUETA 
            {
              Object RESULT = null;
		
    Utilities.verifyCloseEtq(etiquetas, str_etq);

              CUP$Sintax$result = new java_cup.runtime.Symbol(4/*ELEMENTO*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-2)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 13: // ELEMENTO ::= ETIQUETA_INICIO Cierre ELEMENTOINT Apertura CIERRE_ETIQUETA 
            {
              Object RESULT = null;
		
    Utilities.verifyCloseEtq(etiquetas, str_etq);

              CUP$Sintax$result = new java_cup.runtime.Symbol(4/*ELEMENTO*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-4)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 12: // ELEMENTO ::= ELEMENTO ELEMENTOINT 
            {
              Object RESULT = null;
		
    Utilities.verifyCloseEtq(etiquetas, str_etq);

              CUP$Sintax$result = new java_cup.runtime.Symbol(4/*ELEMENTO*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 11: // ELEMENTOINT ::= ETIQUETA_INICIO Cierre Apertura CIERRE_ETIQUETA 
            {
              Object RESULT = null;
		
    Utilities.verifyCloseEtq(etiquetas, str_etq);

              CUP$Sintax$result = new java_cup.runtime.Symbol(7/*ELEMENTOINT*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-3)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 10: // ELEMENTOINT ::= ETIQUETA_INICIO Texto CIERRE_ETIQUETA 
            {
              Object RESULT = null;
		
    Utilities.verifyCloseEtq(etiquetas, str_etq);

              CUP$Sintax$result = new java_cup.runtime.Symbol(7/*ELEMENTOINT*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-2)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 9: // ELEMENTOINT ::= ETIQUETA_INICIO Cierre ELEMENTO Apertura CIERRE_ETIQUETA 
            {
              Object RESULT = null;
		
    Utilities.verifyCloseEtq(etiquetas, str_etq);

              CUP$Sintax$result = new java_cup.runtime.Symbol(7/*ELEMENTOINT*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-4)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 8: // ETIQUETA_INICIO ::= ELEMENTO_ETIQUETA 
            {
              Object RESULT = null;

              CUP$Sintax$result = new java_cup.runtime.Symbol(6/*ETIQUETA_INICIO*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 7: // ETIQUETA_INICIO ::= ELEMENTO_ETIQUETA ATRIBUTO_ETIQUETA 
            {
              Object RESULT = null;

              CUP$Sintax$result = new java_cup.runtime.Symbol(6/*ETIQUETA_INICIO*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 6: // ATRIBUTO_ETIQUETA ::= Atributo 
            {
              Object RESULT = null;

              CUP$Sintax$result = new java_cup.runtime.Symbol(3/*ATRIBUTO_ETIQUETA*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 5: // ATRIBUTO_ETIQUETA ::= Atributo Asignacion TextoC 
            {
              Object RESULT = null;

              CUP$Sintax$result = new java_cup.runtime.Symbol(3/*ATRIBUTO_ETIQUETA*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-2)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 4: // CIERRE_ETIQUETA ::= Barra Etiqueta Cierre 
            {
              Object RESULT = null;
		int etqCloseleft = ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).left;
		int etqCloseright = ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).right;
		Object etqClose = (Object)((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).value;
		
    EtiquetaAux etq = new EtiquetaAux();
    etq.setEtq_name(etqClose.toString());
    etq.setTipo(2);
    str_etq.add(etq);

              CUP$Sintax$result = new java_cup.runtime.Symbol(5/*CIERRE_ETIQUETA*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-2)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 3: // ELEMENTO_ETIQUETA ::= Apertura Etiqueta 
            {
              Object RESULT = null;
		int etqOpenleft = ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).left;
		int etqOpenright = ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right;
		Object etqOpen = (Object)((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).value;
		
    EtiquetaAux etq = new EtiquetaAux();
    etq.setEtq_name(etqOpen.toString());
    etq.setTipo(1);
    str_etq.add(etq);

              CUP$Sintax$result = new java_cup.runtime.Symbol(2/*ELEMENTO_ETIQUETA*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 2: // INICIO ::= ELEMENTO 
            {
              Object RESULT = null;

              CUP$Sintax$result = new java_cup.runtime.Symbol(1/*INICIO*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 1: // INICIO ::= Doctype ELEMENTO 
            {
              Object RESULT = null;

              CUP$Sintax$result = new java_cup.runtime.Symbol(1/*INICIO*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          return CUP$Sintax$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 0: // $START ::= INICIO EOF 
            {
              Object RESULT = null;
		int start_valleft = ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).left;
		int start_valright = ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).right;
		Object start_val = (Object)((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).value;
		RESULT = start_val;
              CUP$Sintax$result = new java_cup.runtime.Symbol(0/*$START*/, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-1)).left, ((java_cup.runtime.Symbol)CUP$Sintax$stack.elementAt(CUP$Sintax$top-0)).right, RESULT);
            }
          /* ACCEPT */
          CUP$Sintax$parser.done_parsing();
          return CUP$Sintax$result;

          default:
            {
              throw new Exception(
                 "Invalid action number found in internal parse table");
            }

        }
    }
}

