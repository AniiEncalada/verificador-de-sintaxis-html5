package sintactico;

import java.io.StringReader;
import java_cup.runtime.Symbol;

public class analizador {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        String cadena = "<div id=\"m-12\">\n" +
                "        <a href=>\n" +
                "            <b id=>\n" +
                "               <strong id=>\n" +
                "                   <span id=>\n" +
                "                   </span>\n" +
                "               </strong>\n" +
                "            </b>\n" +
                "        </a>\n" +
                "</div>";
        // String cadena = "<a href=\"gg\"></a>";

        Sintax s = new Sintax(new LexerCup(new StringReader(cadena)));
        try {
            s.parse();
        } catch (Exception e) {

            Symbol sym = s.getS();
            // System.out.println(sym);
            System.out.println(
                    "Error de sintaxis linea " + (sym.right + 1) + " columna " + (sym.left + 1) + " Texto "
                            + sym.value);
        }
    }
}