/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utilities;

import errors.*;
import java.util.ArrayList;
import java.util.Iterator;
import models.*;

/**
 *
 * @author andre
 */
public class Utilities {

    //Chequear que la etiqueta de apertura sea la misma que la etiqueta de cierre
    public static void verifyCloseEtq(ArrayList<Etiqueta> etiquetas, ArrayList<EtiquetaAux> str_etq) throws NoMatchCloseEtiquetaException {
        System.out.println("size " + str_etq.size());
        for (int i = 0; i < str_etq.size(); i++) {
            System.out.println(str_etq.get(i).toString());
        }
        for (int j = 0; j < str_etq.size(); j++) {
            if (str_etq.get(0).getTipo() != 2) {
                String eval = str_etq.get(0).getEtq_name();
                int pos_eval = 0;
                for (int i = 1; i < str_etq.size(); i++) {
                    if (str_etq.get(i).getTipo() == 2) {
                        if (str_etq.get(i).getEtq_name().equals(eval)) {
                            System.out.println("correcto <" + eval + "></" + str_etq.get(i).getEtq_name() + ">");
                            str_etq.remove(i);
                            str_etq.remove(pos_eval);
                        } else {
                            throw new NoMatchCloseEtiquetaException("Las etiquetas de apertura y cierre no counciden: <" + str_etq.get(i - 1).getEtq_name() + "></" + str_etq.get(i).getEtq_name() + ">");
                        }
                    } else {
                        eval = str_etq.get(i).getEtq_name();
                        pos_eval = i;
                    }
                }
            } else {
                throw new NoMatchCloseEtiquetaException("Las etiquetas de apertura y cierre no counciden: <null></" + str_etq.get(0).getEtq_name() + ">");
            }
        }
    }

    public static void printVariables(ArrayList<Etiqueta> list) {
        for (Iterator<Etiqueta> iterator = list.iterator(); iterator.hasNext();) {
            Etiqueta next = iterator.next();
            System.out.println("apertura =" + next.getApertura() + "; cierre = " + next.getCierre());
        }
    }

    public static boolean checkValue(ArrayList<Integer> variables, String toString) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
