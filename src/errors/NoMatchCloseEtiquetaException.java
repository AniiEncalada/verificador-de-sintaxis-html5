/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Exception.java to edit this template
 */
package errors;

/**
 *
 * @author andre
 */
public class NoMatchCloseEtiquetaException extends Exception {

    /**
     * Creates a new instance of <code>NoVariableFoundException</code> without
     * detail message.
     */
    public NoMatchCloseEtiquetaException() {
    }

    /**
     * Constructs an instance of <code>NoVariableFoundException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public NoMatchCloseEtiquetaException(String msg) {
        super(msg);
    }
}
