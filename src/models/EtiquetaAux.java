/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author alex
 */
public class EtiquetaAux {
    private String etq_name;
    private int nivel;
    private int tipo;

    public String getEtq_name() {
        return etq_name;
    }

    public void setEtq_name(String etq_name) {
        this.etq_name = etq_name;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "EtiquetaAux{" + "etq_name=" + etq_name + ", tipo=" + tipo + '}';
    }
    
}
