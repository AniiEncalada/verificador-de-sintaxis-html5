package sintactico;
import java_cup.runtime.Symbol;
%%
%public
%class LexerCup
%type java_cup.runtime.Symbol
%cup
%full
%line
%char
L=[a-zA-Z_]+
D=[0-9]+
espacio=[ ,\t,\r,\n]+
%{
    private Symbol symbol(int type, Object value){
        return new Symbol(type, yyline, yycolumn, value);
    }
    private Symbol symbol(int type){
        return new Symbol(type, yyline, yycolumn);
    }
%}
%%
/* texto */
(">"(.)*"<") {SymbolTable.addArrayList("TEXTO"); SymbolTable.addArrayList2(yytext()); SymbolTable.addArrayList3(yyline+1); SymbolTable.addArrayList4(yycolumn+1);return new Symbol (sym.Texto, yychar, yyline, yytext());}
/* Espacios en blanco */
{espacio} {/*ignore*/}
/* Comentarios */
("<!--"(.)*"-->") {/*ignore*/} //<!--Texto-->
/* Comillas */
//("\""  |  "\'") {return new Symbol(sym.Comillas, yychar, yyline, yytext());}
//("\""(.)*"\"" | "'"(.)*"'") {/*ignore*/}
("\""(.)*"\"" | "'"(.)*"'") {SymbolTable.addArrayList("TEXTO CON COMILLAS"); SymbolTable.addArrayList2(yytext()); SymbolTable.addArrayList3(yyline+1); SymbolTable.addArrayList4(yycolumn+1);return new Symbol (sym.TextoC, yychar, yyline, yytext());}
/* Asiganación */
("<!DOCTYPE>") {return new Symbol(sym.Doctype, yychar, yyline, yytext());}

/* Etiquetas */
(html  |  head  |  title  |  link  |  meta  |  style  | body  | nav  | main  |  section | article  | aside  | h1  | h2  | h3  | h4  | h5  | h6  |  header  |  footer  |  p  |  hr  | pre  | blockquote  |  ol  | ul  | li  | dl  | dt   |  dd  | figure  | figcaption   | div  | a  |  strong  | small  | cite  | sub  | sup  |  mark  |  span | br  | img   | iframe  | embed  |  object  | video  | audio  |  source | svg  |  table |  caption  | colgroup  | tbody   | thead  | b |  tfoot  |  tr | td  | th  | form  |  fieldset |  legend | label  |  input  | button | select  | option  | textarea ) {SymbolTable.addArrayList("ETIQUETA"); SymbolTable.addArrayList2(yytext()); SymbolTable.addArrayList3(yyline+1); SymbolTable.addArrayList4(yycolumn+1);return new Symbol(sym.Etiqueta, yychar, yyline, yytext());}

/* Atributos */
(accept  |  accept-charset  |  accesskey  |  action  |  align  |  alt  | async  | autocomplete  | autofocus  |  autoplay | bgcolor  | border  | buffered  | charset  | checked  | cite  | "class"  | code  |  color  |  cols  |  colspan  |  content  | contenteditable  | contextmenu  |  controls  | coords  | data  | datetime  | default   |  defer  | dir  | dirname  | disabled  | download  |  draggable  | enctype  | for  | form  | headers  |  height  |  hidden | high  | href   | hreflang  | http-equiv  |  id  | ismap  | itemprop  |  label | lang  |  language |  list  | loop  | low   | max  |  maxlength  |  media | method  | min  | multiple  |  muted |  name | novalidate  |  open  | optimum | pattern  | ping  | placeholder  |  poster |  preload | readonly  |  rel |  required  |  reversed | rows  |  rowspan  |  sandbox |  scope |   seamless |  selected |  size | span  | spellcheck  |  src  |  srcdoc  |  srclang  |  srcset |  start   | style  | target  |  title  |  type |  usemap |  value  | width  | wrap ) {SymbolTable.addArrayList("ATRIBUTO"); SymbolTable.addArrayList2(yytext()); SymbolTable.addArrayList3(yyline+1); SymbolTable.addArrayList4(yycolumn+1);return new Symbol(sym.Atributo, yychar, yyline, yytext());}

/* Espacio */
{espacio} {return new Symbol(sym.ESPACIO, yychar, yyline, yytext());}

/* Asiganación */
("=") {return new Symbol(sym.Asignacion, yychar, yyline, yytext());}

/* Apertura de etiqueta */
("<") {SymbolTable.addArrayList("Apertura"); SymbolTable.addArrayList2(yytext()); SymbolTable.addArrayList3(yyline+1); SymbolTable.addArrayList4(yycolumn+1);return new Symbol(sym.Apertura, yychar, yyline, yytext());}

/* Barra */
("/") {SymbolTable.addArrayList("Barra"); SymbolTable.addArrayList2(yytext()); SymbolTable.addArrayList3(yyline+1); SymbolTable.addArrayList4(yycolumn+1);return new Symbol(sym.Barra, yychar, yyline, yytext());}

/* Cierre de etiqueta */
(">") {SymbolTable.addArrayList("Cierre"); SymbolTable.addArrayList2(yytext()); SymbolTable.addArrayList3(yyline+1); SymbolTable.addArrayList4(yycolumn+1);return new Symbol(sym.Cierre, yychar, yyline, yytext());}

// /* texto */
// ("\'"{L}({L} | {D} | {espacio})*"\'"  |  {L}({L} | {D} | {espacio})*){return new Symbol (sym.Texto, yychar, yyline, yytext());}



/* Error de analisis */
 . {System.out.println("*************** ERROR *********");SymbolTable.addArrayList("error"); SymbolTable.addArrayList2(yytext()); SymbolTable.addArrayList3(yyline+1); SymbolTable.addArrayList4(yycolumn+1);return new Symbol(sym.Error, yychar, yyline, yytext());}